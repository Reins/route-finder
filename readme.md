### Introduction

This is small API application developed as a test-assignment for Sixfold. 
The data used in the application originates from https://openflights.org/data.html (airports.dat & routes.dat) and it has been sanitized a bit.

The application has a single endpoint that calculates shortest route between two different airports:

`GET /api/route/from/{fromCode}/to/{toCode}`

The endpoint has two input parameters which are parsed from path-variables.
- `fromCode`: the starting point (source) airport code in IATA or ICAO format 
- `toCode`: the ending point (destination) airport code in IATA or ICAO format
- Response is a string of the shortest path, for example `A -> B -> C -> D`. When no path was found the response will be 404 with a message `No path found between A -> B`

The endpoint uses a modified implementation of Dijkstra algorithm for finding the shortest path. The distances between airports are calculated using an implementation of the Haversine formula.

The route is restricted with a maximum stops count, which is 5, including the source and destination stops (up to 3 layovers). For example a valid route between A and E could be A -> B -> C -> D -> E.

The maximum stops count could be exceeded if some of the adjacent stops are situated within 100km of each-other. For example if B and C are within 100km, a valid route between A and F could be A -> B => C -> D -> E -> F.

### Application start-up

- Make sure you have Java 11 installed (OpenJDK 11 used to develop the application)
- Run `./gradlew bootRun` in the repository root folder
- Wait about 20-30s, as the initial data-load takes a bit (wait for the following log message: `e.r.routefinder.RoutefinderApplication   : Started RoutefinderApplication in N seconds`)
- Test it out with curl (or a HTTP request editor of your choice). For example when you would be looking to visit your Austrian colleagues and want to find a route between Tallinn and Vienna, you could do: 
`curl http://localhost:8080/api/route/from/TLL/to/VIE`, which would tell you that the shortest possible route would be to connect through Riga airport: `TLL -> RIX -> VIE`

##### _Enjoy!_
