package ee.reins.routefinder.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"iataCode", "icaoCode", "latitude", "longitude"})
public class Airport {
    private String iataCode;
    private String icaoCode;
    private double latitude;
    private double longitude;
}
