package ee.reins.routefinder.domain.graph;

import ee.reins.routefinder.exception.BadRequestException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class AirportsAndConnectionsGraph implements Serializable {
    private final Set<AirportNode> airports = new HashSet<>();

    @SneakyThrows
    public AirportsAndConnectionsGraph deepClone() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(this);

        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream in = new ObjectInputStream(bis);
        return (AirportsAndConnectionsGraph) in.readObject();
    }

    public AirportNode findAirportByCode(String code) {
        if (code == null) {
            throw new BadRequestException("Airport code cannot be null!");
        }
        switch (code.length()) {
            case 3:
                return findAirportByIataCode(code);
            case 4:
                return findAirportByIcaoCode(code);
            default:
                throw new BadRequestException("Unknown airport code format. IATA code should be 3 characters and ICAO code should be 4 characters");
        }
    }

    private AirportNode findAirportByIcaoCode(String code) {
        return airports.stream()
                .filter(airport -> code.equals(airport.getIcaoCode())).findFirst()
                .orElseThrow(() -> new BadRequestException("No airport found with ICAO code " + code));
    }

    private AirportNode findAirportByIataCode(String code) {
        return airports.stream()
                .filter(airport -> code.equals(airport.getIataCode())).findFirst()
                .orElseThrow(() -> new BadRequestException("No airport found with IATA code " + code));
    }

    public void addNode(AirportNode airport) {
        this.airports.add(airport);
    }

    public void addRoute(AirportNode source, AirportNode destination, Double distance) {
        airports.add(source);
        airports.add(destination);

        source.addConnection(new Connection(source.getIataCode(), destination.getIataCode(), distance));
    }

    // dijkstra
    public Optional<String> findShortestPathBetween(AirportNode start, AirportNode destination) {
        HashMap<AirportNode, AirportNode> routeMap = new HashMap<>(); // store hops
        HashMap<String, Double> shortestPathMap = new HashMap<>(); // store shortest path to each node

        routeMap.put(start, null);

        // initialize all possible paths
        airports.forEach(airport -> {
            if (airport == start) shortestPathMap.put(start.getIataCode(), 0.0);
            else shortestPathMap.put(airport.getIataCode(), Double.MAX_VALUE);
        });

        // add connections starting from source airport
        start.getConnections().forEach(connection -> {
            shortestPathMap.put(connection.getDestinationIataCode(), connection.getDistance());
            routeMap.put(findAirportByIataCode(connection.getDestinationIataCode()), start);
        });

        start.visit();

        while (true) {
            AirportNode currentAirport = getClosestUnvisitedAirport(shortestPathMap);

            if (currentAirport == null) {
                log.info("No path found between {} -> {}", start.getIataCode(), destination.getIataCode());
                return Optional.empty();
            }

            log.info("Currently in {}", currentAirport.getIataCode());

            currentAirport.visit();

            if (currentAirport != destination && getCurrentStopCount(currentAirport, routeMap) + 1 >= 6) { // skip path if stop count exceeded
                log.info("Max stop count reached for current path");
                continue;
            }

            if (currentAirport == destination) {
                return Optional.of(formatRoute(destination, routeMap));
            }

            // look through all connections of currentAirport and check if it has better connections than before
            for (Connection connection : currentAirport.getConnections()) {
                if (findAirportByIataCode(connection.getDestinationIataCode()).isVisited()) {
                    continue;
                }

                double routeDistanceWithNextHop = shortestPathMap.get(currentAirport.getIataCode()) + connection.getDistance();
                double currentBestDistanceToDestination = shortestPathMap.get(connection.getDestinationIataCode());

                if (routeDistanceWithNextHop < currentBestDistanceToDestination) {
                    shortestPathMap.put(connection.getDestinationIataCode(), routeDistanceWithNextHop);
                    routeMap.put(findAirportByIataCode(connection.getDestinationIataCode()), currentAirport);
                }
            }
        }
    }

    private int getCurrentStopCount(AirportNode currentAirport, HashMap<AirportNode, AirportNode> routeMap) {
        AirportNode currentStop = currentAirport;
        int stops = 1;
        while (true) {
            AirportNode previousStop = routeMap.get(currentStop);
            log.info("Stops: " + stops + ", currently: " + currentStop.getIataCode());
            if (previousStop == null) break;

            double distanceBetweenPreviousAndCurrent = getDistanceBetweenStops(currentStop, previousStop);

            if (distanceBetweenPreviousAndCurrent > 100.000) {
                log.info("Distance between stops is {}, incrementing stop count", distanceBetweenPreviousAndCurrent);
                stops++;
            }
            currentStop = previousStop;
        }
        return stops;
    }

    private double getDistanceBetweenStops(AirportNode currentStop, AirportNode previousStop) {
        Connection connectionBetweenPreviousAndCurrent = previousStop.getConnections().stream()
                .filter(connection -> connection.getDestinationIataCode().equals(currentStop.getIataCode()))
                .findFirst().orElse(null);
        return connectionBetweenPreviousAndCurrent != null ?
                connectionBetweenPreviousAndCurrent.getDistance() : Double.MAX_VALUE;
    }

    private String formatRoute(AirportNode destination, HashMap<AirportNode, AirportNode> routeMap) {
        AirportNode currentStop = destination;
        StringBuilder path = new StringBuilder(destination.getIataCode());
        while (true) {
            AirportNode previousStop = routeMap.get(currentStop);
            if (previousStop == null) break;

            path.insert(0, previousStop.getIataCode() + " -> ");
            currentStop = previousStop;
        }
        return path.toString();
    }

    private AirportNode getClosestUnvisitedAirport(HashMap<String, Double> shortestPathMap) {
        double currentShortestDistance = Double.MAX_VALUE;
        AirportNode result = null;

        for (AirportNode airportNode : airports) {
            if (airportNode.isVisited()) continue;

            double distanceToAirport = shortestPathMap.get(airportNode.getIataCode());

            if (distanceToAirport < currentShortestDistance) {
                currentShortestDistance = distanceToAirport;
                result = airportNode;
            }
        }

        return result;
    }
}
