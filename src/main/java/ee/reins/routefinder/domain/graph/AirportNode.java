package ee.reins.routefinder.domain.graph;

import lombok.Getter;

import java.io.Serializable;
import java.util.LinkedList;

@Getter
public class AirportNode implements Serializable {
    private final String iataCode;
    private final String icaoCode;
    private final LinkedList<Connection> connections;
    private boolean visited;

    public AirportNode(String iataCode, String icaoCode) {
        this.iataCode = iataCode;
        this.icaoCode = icaoCode;
        this.visited = false;
        this.connections = new LinkedList<>();
    }

    public boolean isVisited() {
        return visited;
    }

    public void visit() {
        this.visited = true;
    }

    public void addConnection(Connection connection) {
        if (!this.connections.contains(connection)) {
            this.connections.add(connection);
        }
    }
}
