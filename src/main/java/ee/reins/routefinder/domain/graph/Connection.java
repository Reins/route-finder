package ee.reins.routefinder.domain.graph;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class Connection implements Serializable {
    private final String sourceIataCode;
    private final String destinationIataCode;
    private final Double distance;
}
