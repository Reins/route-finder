package ee.reins.routefinder.util;

import java.math.BigDecimal;

import static java.lang.Math.*;
import static java.math.RoundingMode.HALF_UP;

public class DistanceUtil {
    private static final double EARTH_RADIUS = 6371;

    public static Double distanceBetween(double fromLatitude, double fromLongitude, double toLatitude, double toLongitude) {
        double distanceLongitude = toRadians(toLongitude - fromLongitude);
        double distanceLatitude = toRadians(toLatitude - fromLatitude);

        // haversine
        double a = pow(sin(distanceLatitude / 2), 2) + cos(toRadians(fromLatitude)) * cos(toRadians(toLatitude)) * pow(sin(distanceLongitude / 2), 2);
        double c = 2 * asin(sqrt(a));

        return formatTo4DecimalPlaces(EARTH_RADIUS * c);
    }

    private static Double formatTo4DecimalPlaces(Double value) {
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(4, HALF_UP);
        return bd.doubleValue();
    }

}
