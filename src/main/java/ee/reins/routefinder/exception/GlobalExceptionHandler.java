package ee.reins.routefinder.exception;

import ee.reins.routefinder.controller.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.ResponseEntity.badRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BadRequestException.class)
    public ResponseEntity<Object> handleException(BadRequestException exception) {
        return badRequest().body(new ErrorResponse(exception.getCustomMessage()));
    }

}
