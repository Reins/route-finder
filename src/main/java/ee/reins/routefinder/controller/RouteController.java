package ee.reins.routefinder.controller;

import ee.reins.routefinder.service.RouteFinderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("api/route")
@RequiredArgsConstructor
public class RouteController {
    private final RouteFinderService routeFinderService;

    @GetMapping("/from/{fromCode}/to/{toCode}")
    public ResponseEntity<?> findRouteBetweenAirports(@PathVariable String fromCode,
                                                      @PathVariable String toCode) {
        Optional<String> route = routeFinderService.findRouteBetweenAirports(fromCode, toCode);
        return route.map(ResponseEntity::ok)
                .orElseGet(() -> new ResponseEntity<>(format("No path found between %s -> %s", fromCode, toCode), NOT_FOUND));
    }

}
