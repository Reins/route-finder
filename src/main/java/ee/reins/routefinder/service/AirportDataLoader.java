package ee.reins.routefinder.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import ee.reins.routefinder.domain.Airport;
import ee.reins.routefinder.domain.Route;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@RequiredArgsConstructor
public class AirportDataLoader {
    private final AirportGraphService airportGraphService;

    @SneakyThrows
    @PostConstruct
    private void initializeData() {
        MappingIterator<Route> routeMappingIterator = new CsvMapper()
                .readerWithTypedSchemaFor(Route.class)
                .readValues(new ClassPathResource("data/routes.csv").getFile());

        List<Route> routes = routeMappingIterator.readAll();

        MappingIterator<Airport> airportMappingIterator = new CsvMapper()
                .readerWithTypedSchemaFor(Airport.class)
                .readValues(new ClassPathResource("data/airports.csv").getFile());

        List<Airport> airports = airportMappingIterator.readAll();

        airportGraphService.initializeAirportGraph(routes, airports);
    }

}
