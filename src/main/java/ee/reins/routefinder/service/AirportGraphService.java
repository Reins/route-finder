package ee.reins.routefinder.service;

import ee.reins.routefinder.domain.Airport;
import ee.reins.routefinder.domain.Route;
import ee.reins.routefinder.domain.graph.AirportNode;
import ee.reins.routefinder.domain.graph.AirportsAndConnectionsGraph;
import ee.reins.routefinder.util.DistanceUtil;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AirportGraphService {
    private final Map<String, Airport> airportMapByIataCode = new HashMap<>();
    private final AirportsAndConnectionsGraph graph = new AirportsAndConnectionsGraph();

    @SneakyThrows
    public AirportsAndConnectionsGraph getNewGraph() {
        return graph.deepClone();
    }

    void initializeAirportGraph(List<Route> routes, List<Airport> airports) {
        airports.forEach(airport -> airportMapByIataCode.put(airport.getIataCode(), airport));

        airports.forEach(airport -> createAirportNode(airport, graph));

        routes.forEach(route -> graph.addRoute(
                graph.findAirportByCode(route.getFrom()),
                graph.findAirportByCode(route.getTo()),
                distanceBetween(route.getFrom(), route.getTo()))
        );
    }

    private void createAirportNode(Airport airport, AirportsAndConnectionsGraph graph) {
        AirportNode airportNode = new AirportNode(airport.getIataCode(), airport.getIcaoCode());
        graph.addNode(airportNode);
    }

    private double distanceBetween(String fromCode, String toCode) {
        Airport start = airportMapByIataCode.get(fromCode);
        Airport finish = airportMapByIataCode.get(toCode);
        return DistanceUtil.distanceBetween(start.getLatitude(), start.getLongitude(), finish.getLatitude(), finish.getLongitude());
    }

}
