package ee.reins.routefinder.service;

import ee.reins.routefinder.domain.graph.AirportNode;
import ee.reins.routefinder.domain.graph.AirportsAndConnectionsGraph;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class RouteFinderService {
    private final AirportGraphService graphService;

    @Cacheable("routesCache")
    public Optional<String> findRouteBetweenAirports(String fromCode, String toCode) {
        AirportsAndConnectionsGraph graph = graphService.getNewGraph();
        AirportNode from = graph.findAirportByCode(fromCode);
        AirportNode to = graph.findAirportByCode(toCode);
        return graph.findShortestPathBetween(from, to);
    }

}
