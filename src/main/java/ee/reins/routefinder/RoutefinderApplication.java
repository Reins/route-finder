package ee.reins.routefinder;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableCaching
public class RoutefinderApplication {

    public static void main(String[] args) {
        run(RoutefinderApplication.class, args);
    }

}
