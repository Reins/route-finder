package ee.reins.routefinder.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.reins.routefinder.service.AirportGraphService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RouteControllerIntegrationTest {
    private final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;
    @SpyBean
    private AirportGraphService graphService;

    @SneakyThrows
    @Test
    public void findsSingleStopRoute() {
        MvcResult result = mockMvc.perform(get("/api/route/from/NYC/to/UKI"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("NYC -> UKI");
    }

    @SneakyThrows
    @Test
    public void findsCorrectRouteWithMultipleStopsAndAlternatives() {
        MvcResult result = mockMvc.perform(get("/api/route/from/NYC/to/LAX"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("NYC -> UKI -> LAX");
    }

    @SneakyThrows
    @Test
    public void returns404AndMessageWhenNoRouteFound() {
        MvcResult result = mockMvc.perform(get("/api/route/from/NYC/to/TLN"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("No path found between NYC -> TLN");
    }

    @SneakyThrows
    @Test
    public void findsCorrectRouteWithIcaoCodes() {
        MvcResult result = mockMvc.perform(get("/api/route/from/NYCI/to/UKIN"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("NYC -> UKI");
    }

    @SneakyThrows
    @Test
    public void respondsFromCacheWhenKnownRoute() {
        mockMvc.perform(get("/api/route/from/NYC/to/UKI"))
                .andExpect(status().isOk())
                .andReturn();

        verify(graphService).getNewGraph();

        mockMvc.perform(get("/api/route/from/NYC/to/UKI"))
                .andExpect(status().isOk())
                .andReturn();

        verifyNoMoreInteractions(graphService);
    }

    @SneakyThrows
    @Test
    public void returnsRelevantErrorWhenUnknownIataCode() {
        MvcResult result = mockMvc.perform(get("/api/route/from/ABC/to/UKI"))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = MAPPER.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.getErrorMessage()).isEqualTo("No airport found with IATA code ABC");
    }

    @SneakyThrows
    @Test
    public void returnsRelevantErrorWhenUnknownIcaoCode() {
        MvcResult result = mockMvc.perform(get("/api/route/from/ABCD/to/UKII"))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = MAPPER.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.getErrorMessage()).isEqualTo("No airport found with ICAO code ABCD");
    }

    @SneakyThrows
    @Test
    public void returnsRelevantErrorWhenUnknownCodeFormat() {
        MvcResult result = mockMvc.perform(get("/api/route/from/ABCDE/to/UKIII"))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = MAPPER.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.getErrorMessage())
                .isEqualTo("Unknown airport code format. IATA code should be 3 characters and ICAO code should be 4 characters");
    }

    @SneakyThrows
    @Test
    public void responds404WhenRouteExceedsThreeLayovers() {
        MvcResult fromOneToSix = mockMvc.perform(get("/api/route/from/ST1/to/ST6"))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(fromOneToSix.getResponse().getContentAsString()).isEqualTo("No path found between ST1 -> ST6");

        MvcResult fromOneToFive = mockMvc.perform(get("/api/route/from/ST1/to/ST5"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(fromOneToFive.getResponse().getContentAsString()).isEqualTo("ST1 -> ST2 -> ST3 -> ST4 -> ST5");

        MvcResult fromTwoToSix = mockMvc.perform(get("/api/route/from/ST2/to/ST6"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(fromTwoToSix.getResponse().getContentAsString()).isEqualTo("ST2 -> ST3 -> ST4 -> ST5 -> ST6");
    }


    @SneakyThrows
    @Test
    public void findsRouteWithMoreLegsWhenShortTransfers() {
        MvcResult result = mockMvc.perform(get("/api/route/from/TLN/to/TÜR"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("TLN -> SAU -> SAK -> KOH -> RAP -> TÜR");
    }

}